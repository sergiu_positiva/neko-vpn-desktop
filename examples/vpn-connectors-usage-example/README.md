# This is a simple demo of using the 'vpn-connectors' module:

## Insallation: 
### Some actions will be required, in order to build this on:
#### Linux/OSX:
- install `make, gcc, g++, g++-multilib (if building for ia32 being on x64), libxml2-dev, libssl-dev, gettext`
- install Python v2.7, v3.5, v3.6, or v3.7
- in folders with libs - chmod 777 vpnc-script
#### Windows:
- Python v2.7, v3.5, v3.6, or v3.7
    - `windows-build-tools` (using __npm install --global --production windows-build-tools__), 
    or
    - Install Visual C++ Build Environment: Visual Studio Build Tools

### How to build:
1. Clone this repo
2. Install NodeJS (version 12.x or lower)
3. Open CMD (on Windows, _!with Administrator privileges!_) or any terminal (on Linux/OSX) and go to the **vpn-connectrs-usage-example** folder.
4. Run *npm install (with sudo, __if on linux__)*. NPM will install all the dependencies required for the app to run.
    1. If building on Windows:
        1. before the step 4, - install windows-build-tools (using __npm install --global --production windows-build-tools__)
        2. possibly you will need Windows 10 SDK in order to properly build the required packages
5. When installation is successfully finished, in cmd/terminal run electron-rebuild (should take some time for building the _ref/ffi_ modules. __If it didn't - retry the step 4__)
    1. On Linux/Mac - sudo ./node_modules/.bin/electron-rebuild
    2. On Windows - .\node_modules\.bin\electron-rebuild.cmd
6. After a successful electron-rebuild type __npm start__. The app should run now!



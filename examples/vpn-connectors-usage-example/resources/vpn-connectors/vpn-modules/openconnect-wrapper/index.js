const path = require('path');
const ffi = require('ffi');
const ref = require('ref');
const structs = require('./structs');
const constants = require('./const');
const fs = require('fs');

let os = process.platform;

function getResponseObject(responseId, message, code) {
  return {
    code: code,
    responseId: responseId,
    message: message
  };
}

var pathAdded = false;

function openconnect() {

  this._ocLib = null;
  this._fd = 0;
  this._vpninfo = null;
  
  var that = this;

  this._getLibsDirectory = function() {
    var res = '';

    if (this._options && this._options.libDirectory) {
      res = path.join(this._options.libDirectory, os);
    } else {
		if (os === 'win32') {
		  res = path.join(__dirname, 'libs', 'win32');
		} else if (os === 'darwin') {
		  res = path.join(__dirname, 'libs', 'mac');
		} else {
		  res = path.join(__dirname, 'libs', 'linux');
		}

		if (res.indexOf('app.asar') > -1 && res.indexOf('app.asar.unpacked') < 0) {
		  res = res.replace('app.asar', 'app.asar.unpacked');
		}
	}
	
    // Temporarly add the path containing the DLLs to ENV PATH variable
    if (!pathAdded) {
      process.env.PATH = `${res}${path.delimiter}${process.env.PATH}`;
      pathAdded = true;
    }

    return res;
  }

  this._getOCLibPath = function() {
    var dllName = '';
    var directory = this._getLibsDirectory();

    if (os === 'win32') {
      dllName = 'libopenconnect-5.dll';
    } else if (os === 'darwin') {
      dllName = 'libopenconnect-5.dylib';
    } else {
      dllName = 'libopenconnect-5.so';
    }

    return path.join(directory, dllName);
  }

  this._getVpncScriptPath = function() {
    var scriptFilename = 'vpnc-script';
    var directory = this._getLibsDirectory();

    if (os === 'win32') {
      scriptFilename += '.js'
    }

    return path.join(directory, scriptFilename);
  }

  this._getSocketLibPath = function() {
    var directory = this._getLibsDirectory();
    var filename = 'socket_wrapper'

    if (os === 'win32') {
      filename += '.dll'
    } else if (os === 'darwin') {
      filename += '.dylib';
    } else {
      filename += '.so';
    }

    return path.join(directory, filename);
  }
  
  this.processAuthFormCallback = ffi.Callback('int', ['void *', ref.refType(structs.ocAuthForm)], processAuthForm);
  this.writeProgressCallback = ffi.Callback('void', ['void *', 'int', 'string', 'pointer'], (vpninfo, level, format, args) => {  });
  this.validatePeerCertCallback = ffi.Callback('int', ['void *', 'string'], (vpninfo, reason) => validatePeerCert(vpninfo, reason));

  this.socketLib = ffi.Library(this._getSocketLibPath(), {
    'pipe_write': ['int', ['int', ref.types.char, 'int']]
  });

  this.initLib = function() {
    return ffi.Library(this._getOCLibPath(), {
      'openconnect_get_version': ["string", []],
      'openconnect_vpninfo_new': ['void *', ['string', 'pointer', 'pointer', 'pointer', 'pointer', 'void *']],
      'openconnect_init_ssl': ['int', []],
      'openconnect_parse_url': ['int', ['void *', 'string']],
      'openconnect_set_reported_os': ['int', ['void *', 'string']],
      'openconnect_set_protocol': ['int', ['void *', 'string']],
      'openconnect_obtain_cookie': ['int', ['void *']],
      'openconnect_set_option_value': ['int', ['void *', 'string']],
      'openconnect_make_cstp_connection': ['int', ['void *']],
      'openconnect_setup_dtls': ['int', ['void *', 'int']],
      'openconnect_setup_tun_device': ['int', ['void *', 'string', 'string']],
      'openconnect_get_port': ['int', ['void *']],
      'openconnect_get_hostname': ['string', ['void *']],
      'openconnect_get_peer_cert_hash': ['string', ['void *']],
      'openconnect_check_peer_cert_hash': ['int', ['void *', 'string']],
      'openconnect_mainloop': ['int', ['void *', 'int', 'int']],
      'openconnect_setup_cmd_pipe': [ref.types.uint, ['void *']],
      'openconnect_vpninfo_free': ['void', ['void *']]
    });
  }

  this._ocLib = this.initLib();

  function validatePeerCert(vpninfo, reason) {
    that._ocLib.openconnect_get_peer_cert_hash(vpninfo);
  }

  function processAuthForm(vpninfo, formBuf) {
    let form = ref.deref(formBuf);
    let opts = form.opts;
  
    while (opts && opts.deref())
    {
      var opt = opts.deref();
  
      if (opt.type == 1 || opt.type == 2) {
        if (opt.name == 'username') {
          if (!that._authOptions.username) {
            throw 'Username required, but is missing';
          }
          else {
            that._ocLib.openconnect_set_option_value(opts, that._authOptions.username);
          }
        } else if (opt.name == 'password') {
          if (!that._authOptions.password) {
            throw 'Password requried, but is missing';
          } else {
            that._ocLib.openconnect_set_option_value(opts, that._authOptions.password);
          }
        } else {
          const unhandledAuthFieldKey = Object.keys(that._authOptions).filter(f => f == opt.name);

          if (!unhandledAuthFieldKey) {
            throw opt.name + ' required, but is missing';
          } else {
            that._ocLib.openconnect_set_option_value(opts, that._authOptions[unhandledAuthFieldKey]);
          }
        }
      }

      opts = opt.next;
    }
  }

  process.on('exit', () => {
    this.writeProgressCallback;
    this.validatePeerCertCallback;
    this.processAuthFormCallback;
  })
};

openconnect.prototype.connect = function(connectionSettings) {
  var res = 0;

  this._connectionSettings = connectionSettings;
  this._options = connectionSettings.options;
  this._authOptions = connectionSettings.authOptions;

  this._vpninfo = this._ocLib.openconnect_vpninfo_new('neko', this.validatePeerCertCallback, ref.NULL, this.processAuthFormCallback, this.writeProgressCallback, ref.NULL);
  
  res = this._ocLib.openconnect_init_ssl();

  if (res)
    return getResponseObject('init_ssl_failed', 'OpenConnect: Failed to setup SSL (errno: ' + res + ')', res);
  
  res = this._ocLib.openconnect_parse_url(this._vpninfo, this._connectionSettings.host);

  if (res)
    return getResponseObject('bad_host', 'OpenConnect: Failed to parse the host URI (errno: ' + res + ')', res);

  var protocol =  this._options && this._options.protocol ? this._connectionSettings.options.protocol : 'anyconnect';
  res = this._ocLib.openconnect_set_protocol(this._vpninfo, protocol);
  
  if (res)
    return getResponseObject('bad_protocol', 'OpenConnect: Failed to set protocol (' + protocol + ') (errno: ' + res + ')', res);

  res = this._ocLib.openconnect_obtain_cookie(this._vpninfo);
  
  if (res)
    return getResponseObject('cookie_fail', 'OpenConnect: Failed to obtain cookie (errno: ' + res + ')', res);

  res = this._ocLib.openconnect_make_cstp_connection(this._vpninfo);

  if (res)
    return getResponseObject('cstp_fail', 'OpenConnect: Failed to establish a CSTP connection (errno: ' + res + ')', res);

  if (this._options && !this._options.disableUdp)
  {
    res = this._ocLib.openconnect_setup_dtls(this._vpninfo, this._options && this._options.dtlsReconnectInterval ? this._options.dtlsReconnectInterval : 16);

    if (res)
      return getResponseObject('udp_fail', 'OpenConnect: Failed to establish a UDP connection (errno: ' + res + ')', res);
  }

  res = this._ocLib.openconnect_setup_tun_device(this._vpninfo, this._getVpncScriptPath(), ref.NULL);

  if (res)
      return getResponseObject('tun_setup_fail', 'OpenConnect: Failed to setup the TUN device (errno: ' + res + ')', res);

  this._fd = this._ocLib.openconnect_setup_cmd_pipe(this._vpninfo);

  if (!this._fd)
    return getResponseObject('cmd_pipe_init_fail', 'OpenConnect: Failed to setup cmd pipe FD', res);

  this._ocLib.openconnect_mainloop.async(this._vpninfo, 300, 10, (err, res) => {
    var errorMsg = null;
    var resMsg = 'Disconnected by client';

    if (err) {
      errorMsg = 'OpenConnect: Unexpected error while establishing connection (' + err + ')';
    }
    else if (res < 0 && res != -constants.EINTR && res != -constants.ECONNABORTED) {
      if (res == -constants.EPIPE) {
        errorMsg = 'Session terminated remotely/explicitly (errno: ' + res + ')';
      } else if (res == -constants.EPERM) {
        errorMsg = 'Unauthorized (cookie expired) (errno: ' + res + ')';
      } else {
        errorMsg = 'Connection closed unexpectedly (errno: ' + res + ')';
      }
    } else if (res == 0) {
      resMsg = 'Connection paused';
    }

    this.ondisconnect(errorMsg ? null : resMsg, errorMsg, res);
    this._ocLib.openconnect_vpninfo_free(this._vpninfo);
  });

  return getResponseObject('ok', 'OK', 0);
};

openconnect.prototype.disconnect = function() {
  this.socketLib.pipe_write(this._fd, constants.OC_CANCEL_CMD, 1);
}

openconnect.prototype.ondisconnect = (successMessage, error, responseCode) => {};

module.exports = openconnect;
const { app, BrowserWindow } = require('electron')

function createWindow () {

  let win = new BrowserWindow({
    width: 400,
    height: 500,
    webPreferences: {
      nodeIntegration: true,
    },
  })

  win.loadFile('index.html')
}

app.on('ready', createWindow)

// module.exports.vpnConnect = function (host, username, password) {
//   var connSettigns = {
//     host: host,
//     authOptions: {
//       username: username,
//       password: password
//     }
//   }

//   vpnconnector.connect(connSettigns);
//   // vpnconnector.connect()
// };
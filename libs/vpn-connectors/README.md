# vpn-connectors

## Node.js bundle of VPN libraries.

This module exports one single function - `createVpn(vpnType)`.
This function instantiates an object, which wraps the logic of connection to required VPN.

The structure of each VPN module is mostly the same, containing the following members to use:

- connect(connectionSettings)
- disconnect()
- ondisconnect - callback, receiving a callback function with the following structure: `function (successMsg, errorMsg, responseCode) { }`

For each VPN module the structure of `connectionSettings` is different.

## Connection settings example:
```
{
    host: *server address*,
    authOptions: {
        username: *username for connection*,
        password: *password for connection*,
        *any other auth param*
        etc...
    },
    options: {
        *for each vpn - different options*
    }
}
```

## Installation:
1. Open **cmd** as Admin or **terminal** (if on Linux/OSX)
2. Go to the project which gonna consume the package
3. Run `sudo (if on Linux) npm install *path to the vpn-connectors folder*`
4. Re-run `npm install`
5. Using [`electron-rebuild`](https://github.com/electron/electron-rebuild) - rebuild the project
6. Start your application.

## Usage example:
```javascript
var vpnConnector = require('vpn-connectors').createVpn('openconnect');

vpnConnector.ondisconnect = function(success, err, code) {
    console.log('success: ', success);
    console.log('error: ', err);
    console.log('response code: ', code);
};

vpnConnector.connect({
    host: *server address*,
    authOptions: {
        username: *username for connection*,
        password: *password for connection*
    }
});
```

## Supported VPN libraries:
- [OpenConnect](https://www.infradead.org/openconnect/) ([doc.](https://bitbucket.org/sergiu_positiva/neko-vpn-desktop/src/master/libs/vpn-connectors/vpn-modules/openconnect-wrapper/))
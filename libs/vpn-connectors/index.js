exports.createVpn = function(vpnType, connectionSettings) {
  switch (vpnType.toUpperCase()) {
    case 'OPENCONNECT':
    {
      var openconnect = require('openconnect-wrapper');

      return ocConnector = new openconnect();
    }
    default:
      throw 'Unknown VPN type';
  }
}
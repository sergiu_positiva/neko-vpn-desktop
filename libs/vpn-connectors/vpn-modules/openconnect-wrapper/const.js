// Response/error codes returned by  mainloop
exports.EPERM = 1; // if the gateway sent 401 Unauthorized (cookie expired)
exports.EINTR = 4 // if aborted locally via OC_CMD_CANCEL;
exports.EPIPE = 32; // if the remote end explicitly terminated the session
exports.ECONNABORTED = 106; //if aborted locally via OC_CMD_DETACH (session aborted/all connections closed)

exports.OC_CANCEL_CMD = 'x';
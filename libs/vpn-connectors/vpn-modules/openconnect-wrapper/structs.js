const ref = require('ref');
const Struct = require('ref-struct');

exports.ocFormOpt = Struct({
  next: 'pointer',
  type: 'int',
  name: 'string',
  label: 'string',
  flags: ref.types.uint,
  reserved: 'void *',
  _value: 'string'
});

exports.ocAuthForm = Struct({
  banner: 'string',
  message: 'string',
  error: 'string',
  auth_id: 'string',
  method: 'string',
  action: 'string',
  opts: ref.refType(this.ocFormOpt),
  authgroup_opt: 'pointer',
  authgroup_selection: ref.types.int
});
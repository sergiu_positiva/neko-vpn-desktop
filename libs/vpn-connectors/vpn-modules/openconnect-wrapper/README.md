# openconnect-wrapper


Node.js wrapper of OpenConnect library ([link](https://www.infradead.org/openconnect/))

### Exports a class with the following members:
- `connect(connectionSettings)`
- `disconnect()`
- `ondisconnect` - callback, receiving a callback function with the following structure: `function (successMsg, errorMsg, responseCode) { }`

### Connection settings object structure:
- Server address
- Auth options: any auth param supported by the *OpenConnect* server
- Options: some configuration fields:
    - protocol - preferred VPN protocol, supported by *OpenConnect* (e.g.: anyconnect, juniper, etc.)
    - disableUdp
    - dtlsReconnectInterval
    - libDirectory - full path to the foloder, containing libs, divided into sub-folders by OS name(e.g.: win32, linux, mac)